from peewee import *

DATABASE = 'sites.db'

db = SqliteDatabase('C://WallNet/db/sites.db')

class Site(Model):
    url = CharField()
    
    class Meta:
       database = db

    
class Note(Model):
    data = CharField()
    site = ForeignKeyField(Site)
    
    class Meta:
       database = db

#db.create_tables([Site,Note])
