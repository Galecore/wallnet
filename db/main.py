from peewee import *
from .classes import *

DATABASE = 'sites.db'
db = SqliteDatabase('C://WallNet/db/sites.db')

#sites.db

## class Site(Model):
##   url = CharField()
##   class Meta:
##       database = db
##
##        
## class Note(Model):
##   data = CharField()
##   site = ForeignKeyField(Site)
##   class Meta:
##       database = db

def create_site(site_url):
    db.connect()
    site = Site(url=site_url)
    site.save()
    db.close()
    return site

def create_note(data, site):
    db.connect()
    note = Note(data=data, site=site)
    note.save()
    db.close()
    return note

def delete_note(note_id):
    db.connect()
    note = Note.select().where(Note.id == note_id)[0]
    note.delete_instance()
    db.close()
    return None

def find_notes(site_url):
    db.connect()
    notes = Note.select().join(Site).where(Site.url == site_url)
    db.close()
    notes = [note.data for note in notes]
    return notes

def url_in_sites(site_url):
	if Site.select().where(Site.url == site_url):
		return True
	return False

##site = create_site('https://developer.chrome.com/extensions/getstarted')
##note = create_note('Hello!', site)
##site = create_site('http://goto.msk.ru/')
##note = create_note('hi', site)
