from flask import Flask
from flask import request

from db import main
from db import classes

import peewee
import json

app = Flask(__name__)

@app.route("/create_note", methods=['POST'])
def create_note():
    url = request.form['url']
    data = request.form['data']
    print(url)
    print(data)
    if main.url_in_sites(url):
        x = classes.Site.select().where(classes.Site.url == url)[0]
        print(x)
        note = main.create_note(data, x)
    else:
        print(0.5)
        site = main.create_site(url)
        print(2)
        main.create_note(data, site)
        print(3)
    return 'Note created'

@app.route('/notes', methods=['GET'])
def notes():
    url = request.args.get('url')
    print(url)
    a = main.find_notes(url)
    return json.dumps(a)

@app.route('/delete', methods=['GET'])
def delete():
    id = request.args.get('id')
    main.delete_note(id)
    return 'Note deleted'

if __name__ == "__main__":
    app.run(host='localhost', port=80)
